
#include <stdlib.h>
#include <memory.h>

#include "polyinterp.h"

int interpolate_linear ( const struct point2d* p, struct point2d* pt )
{
    pt->y = ((p[1].x - pt->x) * p[0].y + (pt->x - p[0].x) * p[1].y) / (p[1].x - p[0].x);
    return 0;
}

int interpolate_linear_line ( struct interp1d* line, const struct point2d* p)
{
    line->m = (p[1].y - p[0].y) / ( p[1].x - p[0].x );
    line->b = p[0].y - line->m * p[0].x;

    return 0;
}

int interpolate_linear_calc ( const struct interp1d* line, struct point2d* pt)
{
    pt->y = line->m * pt->x + line->b;

    return 0;
}

int interpolate_quadratic ( const struct point2d* p, struct point2d* q )
{
    // Use the quadratic Lagrange formula to calculate the value at point q.x
    double x = q->x;
    double L0 = (x - p[1].x)*(x - p[2].x)/( (p[0].x - p[1].x) * (p[0].x - p[2].x) );
    double L1 = (x - p[0].x)*(x - p[2].x)/((p[1].x - p[0].x)*(p[1].x - p[2].x));
    double L2 = (x - p[0].x)*(x - p[1].x)/((p[2].x - p[0].x)*(p[2].x - p[1].x));

    double y = p[0].y*L0 + p[1].y*L1 + p[2].y*L2;
    q->y = y;

    return 0;
}

int interpolate_poly ( const struct point2d* p, int n, struct point2d* q ) {
    // Use Lagrange formula to calculate the value at point q.x
    double x = q->x;

    // Lagrange multipliers
    double *L = malloc(sizeof(double)*n);
    for (int i=0; i<n; i++) {
        L[i] = 1.0;
        for (int j=0; j<n; j++) {
            if (i!=j) {
                L[i] *= (x - p[j].x);
                L[i] /= (p[i].x - p[j].x);
            }
        }
    }

    // Calculate value of y
    double y = 0.0;
    for (int i=0; i<n; i++) {
        y += p[i].y * L[i];
    }

    q->y = y;

    free(L);

    return 0;
}

int interpolate_poly_neville(const struct point2d* p, int n, struct point2d* q) {
    double x = q->x;

    // Setup the array of values and y values and we will calculate the triangular tableau
    double *T = malloc(sizeof(double)*n);
    for (int i = 0; i < n; i++)
        T[i] = p[i].y;

    // Run the Neville algorithm in place
    for (int k = 0; k < n-1; k++) {
        for (int i = 0; i < n - k; i++) {
            int j = i + k + 1;
            T[i] = (x - p[j].x) * T[i] - (x - p[i].x)*T[i + 1];
            T[i] /= (p[i].x - p[j].x);
        }
    }

    q->y = T[0];

    free(T);

    return 0;
}

int interpolate_poly_newton(const struct points2d* p, struct points2d* q) {
    double *x = p->x;
    int n = p->n;

    // Allocate F and copy over values of y
    double *F = malloc(sizeof(double)*n);
    memcpy(F, p->y, sizeof(double)*n);

    // Calculate values of F_x0, F_x1, ..., F_x0...xn
    for (int i=1; i<n; i++) {
        for (int j=n-1; j>=i; j--) {
            F[j] = (F[j] - F[j-1]) / (x[j] - x[j-i]);
        }
    }

    // Evaluate the function at q
    for (int k = 0; k < q->n; k++) {
        double yf = F[n - 1];
        double xf = q->x[k];
        for (int i = n - 2; i >= 0; i--) {
            yf *= (xf - x[i]);
            yf += F[i];
        }
        q->y[k] = yf;
    }

    free(F);

    return 0;
}

// Algorithm for the inverse of Vandermonde matrix taken from paper - 
// "Inverse of the Vandermonde matrix with applications - by Richard Turner, Lewis Research Center, Cleveland OH"
int interpolate_poly_standard_basis(const struct points2d * p, struct interp_nd * f) {
    // Shorter variable renaming. Should be optimized out by compiler
    int n = p->n;
    double* x = p->x;
    double* y = p->y;
    double* a = f->a;

    // Temporary array to store values of the matrix and the matrix vector computation
    double* m = malloc(sizeof(double) * n);
    double* r = malloc(sizeof(double) * n);
    double* Liy = malloc(sizeof(double) * n);

    // Multiply by L inverse
    for (int i = 0; i < n; i++) {

        Liy[i] = 0;

        for (int j = 0; j <= i; j++) {
            if (i == 0 ) {
                Liy[i] = y[0];
            }
            else {
                m[j] = 1.0;

                for (int k = 0; k <= i; k++) {
                    if (k != j) {
                        m[j] *= 1 / (x[j] - x[k]);
                    }
                }

                Liy[i] += m[j] * y[j];
            }
        }
    }
    
    // Multiply by U inverse
    memset(r, 0, sizeof(double) * n);

    for (int i = 0; i < n; i++) {
        a[i] = 0;
        for (int j = i; j < n; j++) {
            if (i == j) {
                m[j] = 1;
            }
            else {
                m[j] = r[j - 1] - m[j - 1] * x[j - 1];
            }
            a[i] += m[j] * Liy[j];
        }
        memcpy(r, m, sizeof(double)*n);
    }

    // Clean up
    free(m);
    free(r);
    free(Liy);

    return 0;
}

int polyeval_horner(const struct interp_nd* f, struct points2d* p) {

    double* x = p->x;
    double* y = p->y;
    double* a = f->a;

    for (int i = 0; i < p->n; i++) {        
        y[i] = a[f->n - 1];
        for (int j = 0; j < f->n-1; j++) {
            int k = f->n - 1 - j;
            y[i] *= x[i];
            y[i] += a[k-1];
        }
    }

    return 0;
}
